import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import Article from '../article';

import './pages.scss';


export default class ContentPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            collapse: ''
        };
    }

    componentDidMount(){
        const { itemProps:{history} } = this.props;

        const stateUpdate = () => {

            // Render closed or opened blocks from url parameters when App is loading
            if ( history.location.search ) {
                let parts = history.location.search.split('=');
                let lastSegment = parts.pop() || parts.pop();
    
                // Update state
                this.setState(({ collapse }) => {
                    if ( collapse.indexOf(lastSegment) ) {
                        return {
                            collapse: lastSegment
                        }
                    }
                });
            }
        };

        stateUpdate();
    }


    render() {

        const { collapse } = this.state;
        const { itemProps:{tabMenu, ...route} } = this.props;
        const { history, location } = route;

        let isCollapse = collapse.split(',');

        // Closes or opens blocks on click to header
        const showToggle = (e) => {
            e.persist();

            // Update state
            this.setState(({ collapse }) => {

                const title = e.target.innerText;

                // If State not empthy add old values fom state and new value from event
                if ( collapse.length && isCollapse.indexOf(title) < 0 ) {
                    isCollapse = [
                        collapse,
                        title
                    ]
                    // If state has same value as value drom event, remove value from state
                } else if ( isCollapse.indexOf(title) > -1 ) {
                    for( let i = 0; i < isCollapse.length; i++){ 
                        if ( isCollapse[i] === title) { 
                            isCollapse.splice(i, 1); 
                        }
                    }
                    // If state is empty add to state just value from event
                } else {
                    isCollapse = [
                        title
                    ]
                }
                
                // Convert array with new values to string
                const urlParam = isCollapse.join();

                // if array is empty render page without parameters
                if ( !isCollapse.length ) {
                    history.push(location.pathname);
                } else {
                    history.push(`${location.pathname}?collapse=${urlParam}`);
                }

                // Update state
                return {
                    collapse: urlParam
                }

            });
        };

        // Just get last owrd in path
        let pathname = route.location.pathname.split('/');
        let exactPath = pathname.pop() || pathname.pop();

        // If we have just one level url redirect to default page
        if ( pathname.length < 2 ) {
            return <Redirect to='/products/general'/>
        }

        // Get exctly one element from array to create article
        const contentData = tabMenu.findIndex((el) => el.id === exactPath);

        const articles = tabMenu[contentData].data.map((article) => {

            const {id} = article;

            return (
                <Article key={id} showToggle={showToggle} collapse={ collapse } {...route} {...article} />
            )
        });

        // Render articles
        return (
            <div className='content-wrapper'>
                {articles}
            </div>
        );
    }
}