import React from 'react';

import AppTabs from '../app-tabs';
import {ContentPage} from '../pages';

import './pages.scss';

const ProductsPage = ({ ...itemProps }) => {

    return (
        <section className="content col-md-10 ml-sm-auto col-lg-10">
            <AppTabs tabMenu={itemProps.tabMenu} itemProps={itemProps}/>
            <ContentPage itemProps={itemProps}></ContentPage>
        </section>
    );
}

export default ProductsPage;