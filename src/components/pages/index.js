import NewsPage from './news-page';
import OrdersPage from './orders-page';
import CustomersPage from './customers-page';
import ProductsPage from './products-page';
import ContentPage from './content-page';

export {
    NewsPage,
    OrdersPage,
    CustomersPage,
    ProductsPage,
    ContentPage
}