import React from 'react';
import { NavLink } from 'react-router-dom';

import './menu-link.scss';

// That Component creates links for entire App
const MenuLink = ({ label, href, itemProps }) => {

    let toHref = '';

    const { match, location } = itemProps;

    // If path has only '/' url creates without 'match path' because '/' from 'match path' and '/' from 'href' creates wrong path '//'
    if ( match.path === '/' ) {
        toHref = `${href}${location.search}`;
    } else {
        toHref = `${match.path}${href}${location.search}`;
    }

    // Render Links
    return (
        <NavLink to={toHref} className="nav-link"
            activeClassName ="active">
        {label}
        </NavLink>
    );
}

export default MenuLink;