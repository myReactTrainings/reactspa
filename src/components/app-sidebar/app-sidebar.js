import React, { Component } from 'react';

import MenuLink from '../menu-link';

import './app-sidebar.scss';

export default class AppSideBar extends Component {

    render() {

        const { sideMenu, ...itemProps } = this.props;

        // Create link for each element in array for Sidebar menu
        const links = sideMenu.map(({ id, ...itemInfo }) => {
            return (
                <li key={id} className="menu-item list-group-item">
                    <MenuLink {...itemInfo} itemProps={itemProps}/>
                </li>
            )
        });

        // Render Sidebar menu
        return (
            <aside className="sidebar col-md-2">
                <nav>
                    <ul className="list-group">
                        {links}
                    </ul>
                </nav>
            </aside>
        );
    }
}