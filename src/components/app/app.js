import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Redirect } from 'react-router';

// Import Components
import AppSideBar from '../app-sidebar';
import {
    NewsPage,
    OrdersPage,
    CustomersPage,
    ProductsPage,
    ContentPage
} from '../pages';


import './app.scss';

export default class App extends Component {

    // Create App state
    state = {
        sideMenu: [
            {label: 'Products', href: '/products/', id: 'products'},
            {label: 'Customers', href: '/customers/', id: 'customers'},
            {label: 'Orders', href: '/orders/', id: 'orders'},
            {label: 'News', href: '/news/', id: 'news'}
        ],
        tabMenu: [
            {
                label: 'General',
                href: 'general',
                isActive: false,
                id: 'general',
                data: [
                    {title: 'Shipping', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art1'},
                    {title: 'Billing', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art2'},
                    {title: 'home', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art3'}
                ]
            },
            {
                label: 'Addresses',
                href: 'addresses',
                isActive: false,
                id: 'addresses',
                data: [
                    {title: 'Address1', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art1'},
                    {title: 'Address2', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art2'}
                ]
            },
            {
                label: 'Orders', 
                href: 'orders', 
                isActive: false, 
                id: 'orders',
                data: [
                    {title: 'Order1', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art1'},
                    {title: 'Order2', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art2'},
                    {title: 'Order3', text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem sapiente quisquam quos fugit officia, quasi sed labore impedit, illum nisi temporibus natus? Doloribus iusto, natus ipsam maxime repellendus dolorum accusantium?', id: 'art3'}
                ]
            }
        ]
    }

    render() {
        // here we use destructuring
        const { sideMenu, ...itemProps } = this.state;

        // Router to use router options as (history, location, match)
         return (
            <div className="app-wrapper d-flex row">
                <Router>
                    <Redirect from='/' to='/products/'/>
                    {/* 
                        @param {object} rProps Parameters from Router(location, history, match).
                     */}
                    <Route render={(rProps) => {
                                return <AppSideBar {...rProps} sideMenu={sideMenu} />
                            }
                    }
                    />
                    <Route path="/news" component={NewsPage}/>
                    <Route path="/customers" component={CustomersPage}/>
                    <Route path="/orders" component={OrdersPage}/>
                    {/* 
                        @param {object} rProps Parameters from Router(location, history, match).
                     */}
                    <Route path="/products/" 
                           render={(rProps) => {
                                // here we use destructuring operator ...rest
                               return <ProductsPage {...rProps} {...itemProps}/>
                            }
                    }
                    />
                    <Route path="/content-page" component={ContentPage}/>
                </Router>
            </div>
         );
    }
}