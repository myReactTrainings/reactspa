import React from 'react';

import './article.scss';

const Article = ({ title, text, showToggle, collapse}) =>  {

    let classNames = 'article-wrapper';

    const isCollapsed = collapse.split(',');
    
    // Add or remove additional class(hide and show) for block if it header exists in url parameters
    classNames += isCollapsed.indexOf(title) > -1 ? ' hide' : ' show';

    // render Article block
    return(
        <article className={classNames}>
            <header onClick={showToggle}>{title}</header>
            <p className="article-content">{text}</p>
        </article>
    );
}

export default Article;