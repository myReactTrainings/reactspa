import React, { Component } from 'react';

import MenuLink from '../menu-link';

import './app-tabs.scss';

export default class AppTabs extends Component {

    render() {

        const { tabMenu, itemProps } = this.props;

        // Create link for each element in array for Tabs menu
        const tabs = tabMenu.map(({ id, ...itemInfo}) => {
            return (
                <li key={id} className="nav-item">
                    <MenuLink itemProps={itemProps} {...itemInfo}/>
                </li>
            )
        });

        // Render Tabs menu
        return(
            <nav className="tabs">
                <ul className="nav nav-tabs">
                    {tabs}
                </ul>
            </nav>
        );
    }
}